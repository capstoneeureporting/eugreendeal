# Generated by Django 3.0.5 on 2020-05-05 16:46

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('airpollution', '0008_auto_20200505_1527'),
    ]

    operations = [
        migrations.DeleteModel(
            name='PollutionByLocation',
        ),
    ]
