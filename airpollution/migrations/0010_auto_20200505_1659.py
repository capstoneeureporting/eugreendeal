# Generated by Django 3.0.5 on 2020-05-05 16:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('airpollution', '0009_delete_pollutionbylocation'),
    ]

    operations = [
        migrations.AlterField(
            model_name='satelliteimagefiles',
            name='image',
            field=models.CharField(blank=True, db_index=True, max_length=3600000),
        ),
    ]
