# Generated by Django 3.0.5 on 2020-05-07 11:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('airpollution', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='chartviz',
            name='label',
            field=models.CharField(blank=True, max_length=2048, null=True),
        ),
    ]
